<?php

namespace Drupal\ztools\Plugin\views\field;

use Drupal\Core\Link;
use Drupal\views\Plugin\views\field\EntityOperations;
use Drupal\views\ResultRow;

/**
 * Renders all operations links for an entity as individual links.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("ztools_entity_operations_links")
 */
class EntityOperationsAsLinks extends EntityOperations {


  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values) {
    $entity = $this->getEntityTranslation($this->getEntity($values), $values);
    $operations = $this->entityTypeManager->getListBuilder($entity->getEntityTypeId())->getOperations($entity);

    if ($this->options['destination']) {
      foreach ($operations as &$operation) {
        if (!isset($operation['query'])) {
          $operation['query'] = [];
        }
        $operation['query'] += $this->getDestinationArray();
      }
    }

    $links = [];
    foreach ($operations as $op) {
      $links[] = Link::fromTextAndUrl(
        $op['title'],
        $op['url']
      );
    }

    return [
      '#theme' => 'item_list',
      '#items' => $links,
    ];
  }


}
